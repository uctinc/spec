package spec

import (
	"errors"
	"net"
	"regexp"
	"strings"
)

var AllocatableRange = net.IPNet{
	IP:   net.ParseIP("172.24.0.0"),
	Mask: net.IPv4Mask(255, 248, 0, 0),
}

type Allocation struct {
	Subnet      ipnet  `toml:"Subnet"`
	Owner       string `toml:"Owner"`
	SubnetName  string `toml:"SubnetName"`
	IntendedUse string `toml:"IntendedUse"`
}

var RegisterableFQDNPattern = regexp.MustCompile("^(?:[a-z][a-z0-9\\-]{0,22}[a-z0-9]\\.)*([a-z][a-z0-9\\-]{3,22}[a-z0-9])\\.uctinc\\.net$")

var SubnetInvalidRange = errors.New("Subnet sits outside of the valid allocatable range.")
var SubnetInvalidSize = errors.New("Subnet is of incorrect size.")
var OwnerMalformed = errors.New("Owner is malformed and does not conform.")
var SubnetNameInvalid = errors.New("Subnet name is invalid.")
var IntendedUseEmpty = errors.New("Intended use is empty.")

var FreeSubnetHasSubnetName = errors.New("A subnet marked as owned by Free has a name.")
var FreeSubnetHasIntendedUse = errors.New("A subnet marked as owned by Free has an intended use.")

var AllocatedSubnetUsesReservedNameInproperly = errors.New("A subnet marked as owned by one member has attempted to use a head hostname formatted to masquarade as another potential member.")

var AllocatedSubnetViolatesSubSuperAllocationRules = errors.New("A collision was detected where one subnet's name shared a head hostname with another subnet where the owners are not the same member.")

func (a Allocation) Validate(hostsSet map[string]string) error {
	// at this level, we can only validate that the subnet is within our
	// allocatable range

	if !AllocatableRange.Contains(a.Subnet.IP) {
		return SubnetInvalidRange
	}

	ones, bits := a.Subnet.Mask.Size()

	if ones != 22 || bits != 32 {
		return SubnetInvalidSize
	}

	if a.Owner == "Free" {
		// a free allocation should NOT have a name or use
		if len(a.SubnetName) != 0 {
			return FreeSubnetHasSubnetName
		} else if len(a.IntendedUse) != 0 {
			return FreeSubnetHasIntendedUse
		}
	} else if a.Owner == "Reserved" {
		// a reserved allocation must have a use
		if len(a.IntendedUse) == 0 {
			return IntendedUseEmpty
		}

		// We do not check subnet names specifically.. this is a gray area in
		// the spec that has yet to be defined properly.
	} else {
		if !MemberIdPattern.MatchString(a.Owner) {
			return OwnerMalformed
		}

		match := RegisterableFQDNPattern.FindStringSubmatch(a.SubnetName)
		if match == nil {
			return SubnetNameInvalid
		}

		// reserved names
		if MemberIdPattern.MatchString(strings.ToUpper(match[1])) &&
			strings.Compare(match[1][4:], a.Owner[4:]) != 0 {
			return AllocatedSubnetUsesReservedNameInproperly
		}

		// allocation collisions
		previousOwner, has := hostsSet[match[1]]
		if has && strings.Compare(previousOwner, a.Owner) != 0 {
			return AllocatedSubnetViolatesSubSuperAllocationRules
		}

		hostsSet[match[1]] = a.Owner

		if len(a.IntendedUse) == 0 {
			return IntendedUseEmpty
		}
	}
	return nil
}
