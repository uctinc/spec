# Spec

This package includes the configuration specification for data files governing
uctinc.net. Each data structure contains a validation function which may be used
to weakly validate rules.
