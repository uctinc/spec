package spec

import (
	"errors"
	"net/mail"
	"regexp"
)

type Membership struct {
	MemberId        string `toml:"MemberID"`
	Identity        string `toml:"Identity"`
	ContactEmail    string `toml:"ContactEmail"`
	NetworkOperator bool   `toml:"NetworkOperator"`
}

var MemberIdPattern = regexp.MustCompile("^UCTM[0-9]{3}$")

var MemberIdMalformed = errors.New("MemberId is malformed and does not conform.")
var IdentityEmpty = errors.New("Identity is empty.")
var ContactEmailInvalid = errors.New("ContactEmail is invalid.")

func (m Membership) Validate() error {
	if !MemberIdPattern.MatchString(m.MemberId) {
		return MemberIdMalformed
	}

	if len(m.Identity) <= 0 {
		return IdentityEmpty
	}

	_, e := mail.ParseAddress(m.ContactEmail)
	if e != nil {
		return ContactEmailInvalid
	}

	return nil
}
