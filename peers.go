package spec

import (
	"errors"
	v "github.com/asaskevich/govalidator"
)

type Peer struct {
	Owner        string `toml:"Owner"`
	Identifier   string `toml:"Identifier"`
	Address      string `toml:"Address"`
	SubnetBehind ipnet  `toml:"SubnetBehind"`
}

var IdentifierEmpty = errors.New("Identifier is empty.")
var MalformedAddress = errors.New("Address is malformed.")

func (p Peer) Validate() error {
	if !MemberIdPattern.MatchString(p.Owner) {
		return MemberIdMalformed
	}

	if len(p.Identifier) <= 0 {
		return IdentifierEmpty
	}

	if !v.IsDialString(p.Address) && !v.IsHost(p.Address) {
		return MalformedAddress
	}

	if !AllocatableRange.Contains(p.SubnetBehind.IP) {
		return SubnetInvalidRange
	}

	return nil
}
