package spec

import (
	"errors"
	"net"
)

type ipnet struct {
	net.IPNet
}

var CIDRIPDoesntMatchNet = errors.New("IPV4 CIDR's IP portion does not match the network which it represents.")

func (i *ipnet) UnmarshalText(text []byte) error {
	ip, inet, e := net.ParseCIDR(string(text))
	i.IPNet = *inet

	if e != nil {
		return e
	}

	if !i.IP.Equal(ip) {
		return CIDRIPDoesntMatchNet
	}

	return nil
}
